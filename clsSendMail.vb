﻿Option Strict Off
Option Explicit On
Imports System

Public Class clsSendMail
   Public gMailBody As String
   Public gMailSubject As String
   Public gMailFrom As String
   Public gMailName As String
   Public gSMTPPort As String
   Public gSMTPAccount As String
   Public gSMTPPassword As String
   Public gSMTPSecured As String

   Public g_asMailTo(), g_asCCMail(), g_asBCCMail() As String
   Public g_iMailTo, g_iCCMail, g_iBCCMail As Integer

   Private sErrMsg As String

   Public Function SendOneMail(ByVal sMailFrom As String, ByVal sMailTo As String, ByVal sMailSubj As String, ByVal sMailBody As String, Optional ByVal sAttachment As String = "") As Boolean
      Dim smtpHost As String
      Dim smtpPort, smtpAcct, smtpPwd, smtpSSL As String

      SendOneMail = False

      ' message subject
      smtpHost = My.Settings.SmtpHost
      smtpAcct = My.Settings.SmtpUserID
      smtpPwd = My.Settings.SmtpPwd
      smtpPort = My.Settings.SmtpPort
      smtpSSL = My.Settings.SecuredSSL

      'Use this for office365
      'smtp.UseDefaultCredentials = False
      'smtp.Credentials = New NetworkCredential("myEmail@mydomain.com", "MyPassword")
      'smtp.Host = "smtp.office365.com"
      'smtp.port = 587
      'smtp.enablessl = True

      'Use this for Gmail
      'smtp.Credentials = New NetworkCredential("myEmail@mydomain.com", "MyPassword")
      'smtp.Host = "smtp.gmail.com"
      'smtp.port = 587
      'smtp.enablessl = True

      Try
         Dim objMessage As New System.Net.Mail.MailMessage(sMailFrom, sMailTo, sMailSubj, sMailBody)
         Dim iTmp As Integer

         For iTmp = 0 To g_iCCMail - 1
            objMessage.CC.Add(g_asCCMail(iTmp))
         Next
         For iTmp = 0 To g_iBCCMail - 1
            objMessage.Bcc.Add(g_asBCCMail(iTmp))
         Next

         If Not String.IsNullOrEmpty(sAttachment) Then
            objMessage.Attachments.Add(New System.Net.Mail.Attachment(sAttachment))
         End If

         Dim client As New System.Net.Mail.SmtpClient(smtpHost, smtpPort)
         client.Credentials = New System.Net.NetworkCredential(smtpAcct, smtpPwd)
         client.EnableSsl = smtpSSL
         client.Send(objMessage)
         sErrMsg = "Email has been sent to: " & sMailTo
         SendOneMail = True
      Catch ex As Exception
         sErrMsg = "***** Error sending email to: " & sMailTo & vbCrLf
         sErrMsg = sErrMsg & "---> Desc: " & ex.Message()
      End Try

   End Function

   Public Function SendMails(ByVal sMailFrom As String, ByVal sMailTo As String, ByVal sMailSubj As String, ByVal sMailBody As String, Optional ByVal sAttachment As String = "") As Boolean
      Dim smtpHost As String
      Dim smtpPort, smtpAcct, smtpPwd, smtpSSL As String

      SendMails = False

      ' message subject
      smtpHost = My.Settings.SmtpHost
      smtpAcct = My.Settings.SmtpUserID
      smtpPwd = My.Settings.SmtpPwd
      smtpPort = My.Settings.SmtpPort
      smtpSSL = My.Settings.SecuredSSL

      Try
         Dim objMessage As New System.Net.Mail.MailMessage()
         Dim iTmp As Integer

         objMessage.From = New System.Net.Mail.MailAddress(sMailFrom)
         If sMailTo > " " Then
            objMessage.To.Add(sMailTo)
         Else
            For iTmp = 0 To g_iMailTo - 1
               objMessage.To.Add(g_asMailTo(iTmp))
            Next
         End If
         For iTmp = 0 To g_iCCMail - 1
            objMessage.CC.Add(g_asCCMail(iTmp))
         Next

         For iTmp = 0 To g_iCCMail - 1
            objMessage.CC.Add(g_asCCMail(iTmp))
         Next
         For iTmp = 0 To g_iBCCMail - 1
            objMessage.Bcc.Add(g_asBCCMail(iTmp))
         Next

         objMessage.Subject = sMailSubj
         objMessage.Body = sMailBody
         If Not String.IsNullOrEmpty(sAttachment) Then
            objMessage.Attachments.Add(New System.Net.Mail.Attachment(sAttachment))
         End If

         Dim client As New System.Net.Mail.SmtpClient(smtpHost, smtpPort)
         client.Credentials = New System.Net.NetworkCredential(smtpAcct, smtpPwd)
         client.EnableSsl = smtpSSL
         client.Send(objMessage)
         sErrMsg = "Email has been sent to: " & sMailTo
         SendMails = True
      Catch ex As Exception
         sErrMsg = "***** Error sending email to: " & sMailTo & vbCrLf
         sErrMsg = sErrMsg & "---> Desc: " & ex.Message()
      End Try

   End Function

   Public Function SendMailEx(ByVal sMailFrom As String, ByVal sMailTo As String, ByVal sMailToName As String, ByVal sMailSubj As String, ByVal sMailBody As String, Optional ByVal sAttachment As String = "") As Boolean
      Dim smtpHost, smtpPort, smtpAcct, smtpPwd, smtpSSL As String
      Dim iTmp As Integer

      SendMailEx = False

      ' message subject
      smtpHost = My.Settings.SmtpHost
      smtpAcct = My.Settings.SmtpUserID
      smtpPwd = My.Settings.SmtpPwd
      smtpPort = My.Settings.SmtpPort
      smtpSSL = My.Settings.SecuredSSL

      Try
         Dim mTo As New System.Net.Mail.MailAddress(sMailTo, sMailToName)
         Dim mFrom As New System.Net.Mail.MailAddress(gMailFrom)

         Dim objMessage As New System.Net.Mail.MailMessage(mFrom, mTo)

         objMessage.Subject = sMailSubj
         objMessage.Body = sMailBody

         For iTmp = 0 To g_iCCMail - 1
            objMessage.CC.Add(g_asCCMail(iTmp))
         Next
         For iTmp = 0 To g_iBCCMail - 1
            objMessage.Bcc.Add(g_asBCCMail(iTmp))
         Next

         If Not String.IsNullOrEmpty(sAttachment) Then
            objMessage.Attachments.Add(New System.Net.Mail.Attachment(sAttachment))
         End If

         Dim client As New System.Net.Mail.SmtpClient(smtpHost, smtpPort)
         client.Credentials = New System.Net.NetworkCredential(smtpAcct, smtpPwd)
         client.EnableSsl = smtpSSL
         client.Send(objMessage)
         sErrMsg = "Email has been sent to: " & sMailTo
         SendMailEx = True
      Catch ex As Exception
         sErrMsg = "***** Error sending email to: " & sMailTo & vbCrLf
         sErrMsg = sErrMsg & "---> Desc: " & ex.Message()
      End Try

   End Function

   'Send mails using preconfigured addresses
   Public Function doSendMail(ByVal sMailSubj As String, ByVal sMailBody As String, Optional ByVal sAttachment As String = "") As Boolean
      Dim smtpHost, smtpPort, smtpAcct, smtpPwd, smtpSSL As String
      Dim iTmp As Integer

      doSendMail = False

      'Check  mailto addr
      If g_iMailTo < 1 Then
         sErrMsg = "***** Error: Mailto has not been setup.  Please check config file."
         Exit Function
      End If

      ' message subject
      smtpHost = My.Settings.SmtpHost
      smtpAcct = My.Settings.SmtpUserID
      smtpPwd = My.Settings.SmtpPwd
      smtpPort = My.Settings.SmtpPort
      smtpSSL = My.Settings.SecuredSSL

      Try
         Dim mTo As New System.Net.Mail.MailAddress(g_asMailTo(0))
         Dim mFrom As New System.Net.Mail.MailAddress(gMailFrom)

         Dim objMessage As New System.Net.Mail.MailMessage(mFrom, mTo)

         objMessage.Subject = sMailSubj
         objMessage.Body = sMailBody

         For iTmp = 1 To g_iMailTo - 1
            objMessage.To.Add(g_asMailTo(iTmp))
         Next
         For iTmp = 0 To g_iCCMail - 1
            objMessage.CC.Add(g_asCCMail(iTmp))
         Next
         For iTmp = 0 To g_iBCCMail - 1
            objMessage.Bcc.Add(g_asBCCMail(iTmp))
         Next

         If Not String.IsNullOrEmpty(sAttachment) Then
            objMessage.Attachments.Add(New System.Net.Mail.Attachment(sAttachment))
         End If

         Dim client As New System.Net.Mail.SmtpClient(smtpHost, smtpPort)
         client.Credentials = New System.Net.NetworkCredential(smtpAcct, smtpPwd)
         client.EnableSsl = smtpSSL
         client.Send(objMessage)
         sErrMsg = "Email has been sent to: " & My.Settings.MailTo
         doSendMail = True
      Catch ex As Exception
         sErrMsg = "***** Error sending email to: " & My.Settings.MailTo & vbCrLf
         sErrMsg = sErrMsg & "---> Desc: " & ex.Message()
      End Try

   End Function

   Public Function getLastErrorMsg() As String
      getLastErrorMsg = sErrMsg
   End Function
End Class
