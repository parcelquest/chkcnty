Option Strict Off
Option Explicit On

Friend Class clsZip
   Private hZip As Integer
   Private hUnzip As Integer
   Private sError As String

   Public ZipFilename As String
   Public FilesToZip As String
   Public FilesToExclude As String
   Public UnzipFolder As String

   Public Function getErrMsg() As String
      getErrMsg = sError
   End Function

   Public Function zipIt(ByRef bInclSubfolder As Boolean) As Boolean
      Dim bRet As Boolean
      Dim lResult As Integer
      Dim sDesc As String

      sError = ""
      bRet = False
      If hZip = 0 Then
         ' Get a handle on an Xceed Zip instance
         hZip = XzCreateXceedZipA("SFX50-YZZ3C-E4WJP-Y4CA")

         ' Set the properties
         Call XzSetZipFilenameA(hZip, ZipFilename)
         Call XzSetFilesToProcessA(hZip, FilesToZip)
         Call XzSetProcessSubfolders(hZip, bInclSubfolder)
         Call XzSetFilesToExcludeA(hZip, FilesToExclude)
         'If include subfolder is true, we need to preserve path to avoid conflict
         Call XzSetPreservePaths(hZip, bInclSubfolder)

         ' Zip!
         Try
            lResult = XzZip(hZip)
            If lResult = xerSuccess Then
               bRet = True
            Else
               sDesc = Space(200)
               Call XzGetErrorDescriptionA(hZip, xvtError, lResult, sDesc, 200)
               sError = "***** Zip error: " & Trim(sDesc)
            End If
         Catch ex As Exception
            sError = "***** Exception in ZipIt: " & ex.Message()
         End Try

         ' Free instance
         Call XzDestroyXceedZip(hZip)
         hZip = 0
      Else
         sError = "Already zipping... This application cannot zip files concurrently."
      End If
      zipIt = bRet
   End Function

   'Zip a list of files
   Public Function zipList(ByRef asFileList() As String, ByRef iCount As Integer) As Boolean
      Dim bRet As Boolean
      Dim iTmp As Integer
      Dim lResult As Integer
      Dim sDesc As String

      sError = ""
      bRet = False
      If hZip = 0 Then
         ' Get a handle on an Xceed Zip instance
         hZip = XzCreateXceedZipA("SFX50-YZZ3C-E4WJP-Y4CA")

         ' Set the properties
         Call XzSetZipFilenameA(hZip, ZipFilename)
         'Call XzSetFilesToProcessA(hZip, FilesToZip)
         For iTmp = 0 To iCount - 1
            Call XzAddFilesToProcessA(hZip, asFileList(iTmp))
         Next

         Call XzSetProcessSubfolders(hZip, False)
         Call XzSetFilesToExcludeA(hZip, "")
         'Preserve path to avoid conflict
         Call XzSetPreservePaths(hZip, True)

         ' Zip!
         Try
            lResult = XzZip(hZip)
            If lResult = xerSuccess Then
               bRet = True
            Else
               sDesc = Space(200)
               Call XzGetErrorDescriptionA(hZip, xvtError, lResult, sDesc, 200)
               sError = "***** Zip error: " & Trim(sDesc)
            End If
         Catch ex As Exception
            sError = "***** Exception in ZipIt: " & ex.Message()
         End Try

         ' Free instance
         Call XzDestroyXceedZip(hZip)
         hZip = 0
      Else
         sError = "Already zipping... This application cannot zip files concurrently."
      End If
      zipList = bRet
   End Function

   Public Sub doAbort()
      If hZip <> 0 Then
         Call XzSetAbort(hZip, True)
      End If
      If hUnzip <> 0 Then
         Call XzSetAbort(hUnzip, True)
      End If
   End Sub

   Public Function unzipIt(ByRef bInclSubfolder As Boolean) As Boolean
      Dim bRet As Boolean

      sError = ""
      bRet = False

      Dim lResult As Integer
      Dim sDesc As String
      'Dim cb As FPtr

      If hUnzip = 0 Then
         ' Get a handle on an Xceed Zip instance
         hUnzip = XzCreateXceedZipA("SFX50-YZZ3C-E4WJP-Y4CA")

         ' Set the properties
         Call XzSetZipFilenameA(hUnzip, ZipFilename)
         Call XzSetFilesToProcessA(hUnzip, FilesToZip)
         Call XzSetFilesToExcludeA(hUnzip, "")
         Call XzSetProcessSubfolders(hUnzip, bInclSubfolder)
         Call XzSetUnzipToFolderA(hUnzip, UnzipFolder)

         ' Set the callback function to receive Xceed Zip events
         'cb = AddressOf XceedZipCallbackProc
         'Call LibWrap.XzSetXceedZipCallback(hUnzip, cb)

         ' Unzip!
         Try
            lResult = XzUnzip(hUnzip)
            If lResult = xerSuccess Then
               bRet = True
            Else
               sDesc = Space(200)
               Call XzGetErrorDescriptionA(hUnzip, xvtError, lResult, sDesc, 200)
               sError = "***** Unzip error: " & Trim(sDesc)
            End If
         Catch ex As Exception
            sError = "***** Exception in UnzipIt: " & ex.Message()
         End Try

         ' Free instance
         Call XzDestroyXceedZip(hUnzip)
         hUnzip = 0
      Else
         sError = "*** Already zipping... This application cannot unzip files concurrently."
      End If
      unzipIt = bRet
   End Function

   Public Sub New()
      MyBase.New()
      Call XceedZipInitDLL()
      sError = ""
      hZip = 0
      hUnzip = 0
   End Sub

   Protected Overrides Sub Finalize()
      Call XceedZipShutdownDLL()
      MyBase.Finalize()
   End Sub
End Class