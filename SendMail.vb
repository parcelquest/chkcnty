Option Strict Off
Option Explicit On
Imports System
Imports System.Diagnostics

Module SendMail
   Public gMailBody As String
   Public gMailSubject As String
   Public gMailFrom As String
   Public gMailName As String
   Public gSMTPPort As String
   Public gSMTPAccount As String
   Public gSMTPPassword As String
   Public gSMTPSecured As String

   Public g_asMailTo(), g_asCCMail(), g_asBCCMail() As String
   Public g_iMailTo, g_iCCMail, g_iBCCMail As Integer

   Public Function mySendMail(ByVal sMailTo As String, Optional ByVal sAttachment As String = "") As Boolean
      Dim strTmp, strSubj, smtpHost As String

      mySendMail = False

      ' message subject
      smtpHost = My.Settings.SmtpHost
      strSubj = My.Application.Info.AssemblyName & " - " & My.Computer.Name & ": " & gMailSubject

      Try
         Dim objMessage As New System.Net.Mail.MailMessage(gMailFrom, sMailTo, strSubj, gMailBody)

         If Not String.IsNullOrEmpty(sAttachment) Then
            objMessage.Attachments.Add(New System.Net.Mail.Attachment(sAttachment))
         End If

         Dim client As New System.Net.Mail.SmtpClient(smtpHost, gSMTPPort)
         client.Credentials = New System.Net.NetworkCredential(gSMTPAccount, gSMTPPassword)
         client.EnableSsl = gSMTPSecured

         client.Send(objMessage)
         LogMsg("Email has been sent to: " & sMailTo)
         mySendMail = True
      Catch ex As Exception
         Dim ex2 As Exception = ex
         strTmp = ""
         While Not (ex2 Is Nothing)
            strTmp += ex2.ToString()
            ex2 = ex2.InnerException
         End While

         LogMsg("***** Error sending email to: " & sMailTo)
         LogMsg("---> Desc: " & strTmp)
      End Try

   End Function

   Public Function SendOneMail(ByVal sMailFrom As String, ByVal sMailTo As String, ByVal sMailSubj As String, ByVal sMailBody As String, Optional ByVal sAttachment As String = "") As Boolean
      Dim smtpHost As String
      Dim smtpPort, smtpAcct, smtpPwd, smtpSSL As String

      SendOneMail = False

      ' message subject
      smtpHost = My.Settings.SmtpHost
      smtpAcct = My.Settings.SmtpUserID
      smtpPwd = My.Settings.SmtpPwd
      smtpPort = My.Settings.SmtpPort
      smtpSSL = My.Settings.SecuredSSL

      'Use this for office365
      'smtp.UseDefaultCredentials = False
      'smtp.Credentials = New NetworkCredential("myEmail@mydomain.com", "MyPassword")
      'smtp.Host = "smtp.office365.com"
      'smtp.port = 587
      'smtp.enablessl = True

      'Use this for Gmail
      'smtp.Credentials = New NetworkCredential("myEmail@mydomain.com", "MyPassword")
      'smtp.Host = "smtp.gmail.com"
      'smtp.port = 587
      'smtp.enablessl = True

      Try
         Dim objMessage As New System.Net.Mail.MailMessage(sMailFrom, sMailTo, sMailSubj, sMailBody)
         Dim iTmp As Integer

         For iTmp = 0 To g_iCCMail - 1
            objMessage.CC.Add(g_asCCMail(iTmp))
         Next
         For iTmp = 0 To g_iBCCMail - 1
            objMessage.Bcc.Add(g_asBCCMail(iTmp))
         Next

         If Not String.IsNullOrEmpty(sAttachment) Then
            objMessage.Attachments.Add(New System.Net.Mail.Attachment(sAttachment))
         End If

         Dim client As New System.Net.Mail.SmtpClient(smtpHost, smtpPort)
         client.Credentials = New System.Net.NetworkCredential(smtpAcct, smtpPwd)
         client.EnableSsl = smtpSSL
         client.Send(objMessage)
         LogMsg("Email has been sent to: " & sMailTo)
         SendOneMail = True
      Catch ex As Exception
         LogMsg("***** Error sending email to: " & sMailTo)
         LogMsg("---> Desc: " & ex.Message())
      End Try

   End Function

   Public Function SendMails(ByVal sMailFrom As String, ByVal sMailTo As String, ByVal sMailSubj As String, ByVal sMailBody As String, Optional ByVal sAttachment As String = "") As Boolean
      Dim smtpHost As String
      Dim smtpPort, smtpAcct, smtpPwd, smtpSSL As String

      SendMails = False

      ' message subject
      smtpHost = My.Settings.SmtpHost
      smtpAcct = My.Settings.SmtpUserID
      smtpPwd = My.Settings.SmtpPwd
      smtpPort = My.Settings.SmtpPort
      smtpSSL = My.Settings.SecuredSSL

      Try
         Dim objMessage As New System.Net.Mail.MailMessage()
         Dim iTmp As Integer

         objMessage.From = New System.Net.Mail.MailAddress(sMailFrom)
         If sMailTo > " " Then
            objMessage.To.Add(sMailTo)
         Else
            For iTmp = 0 To g_iMailTo - 1
               objMessage.To.Add(g_asMailTo(iTmp))
            Next
         End If
         For iTmp = 0 To g_iCCMail - 1
            objMessage.CC.Add(g_asCCMail(iTmp))
         Next

         For iTmp = 0 To g_iCCMail - 1
            objMessage.CC.Add(g_asCCMail(iTmp))
         Next
         For iTmp = 0 To g_iBCCMail - 1
            objMessage.Bcc.Add(g_asBCCMail(iTmp))
         Next

         objMessage.Subject = sMailSubj
         objMessage.Body = sMailBody
         If Not String.IsNullOrEmpty(sAttachment) Then
            objMessage.Attachments.Add(New System.Net.Mail.Attachment(sAttachment))
         End If

         Dim client As New System.Net.Mail.SmtpClient(smtpHost, smtpPort)
         client.Credentials = New System.Net.NetworkCredential(smtpAcct, smtpPwd)
         client.EnableSsl = smtpSSL
         client.Send(objMessage)
         LogMsg("Email has been sent to: " & sMailTo)
         SendMails = True
      Catch ex As Exception
         LogMsg("***** Error sending email to: " & sMailTo)
         LogMsg("---> Desc: " & ex.Message())
      End Try

   End Function

   Public Function SendMailEx(ByVal sMailFrom As String, ByVal sMailTo As String, ByVal sMailToName As String, ByVal sMailSubj As String, ByVal sMailBody As String, Optional ByVal sAttachment As String = "") As Boolean
      Dim smtpHost, smtpPort, smtpAcct, smtpPwd, smtpSSL As String
      Dim iTmp As Integer

      SendMailEx = False

      ' message subject
      smtpHost = My.Settings.SmtpHost
      smtpAcct = My.Settings.SmtpUserID
      smtpPwd = My.Settings.SmtpPwd
      smtpPort = My.Settings.SmtpPort
      smtpSSL = My.Settings.SecuredSSL

      Try
         Dim mTo As New System.Net.Mail.MailAddress(sMailTo, sMailToName)
         Dim mFrom As New System.Net.Mail.MailAddress(gMailFrom)

         Dim objMessage As New System.Net.Mail.MailMessage(mFrom, mTo)

         objMessage.Subject = sMailSubj
         objMessage.Body = sMailBody

         'objMessage.To.Add
         For iTmp = 0 To g_iCCMail - 1
            objMessage.CC.Add(g_asCCMail(iTmp))
         Next
         For iTmp = 0 To g_iBCCMail - 1
            objMessage.Bcc.Add(g_asBCCMail(iTmp))
         Next

         If Not String.IsNullOrEmpty(sAttachment) Then
            objMessage.Attachments.Add(New System.Net.Mail.Attachment(sAttachment))
         End If

         Dim client As New System.Net.Mail.SmtpClient(smtpHost, smtpPort)
         client.Credentials = New System.Net.NetworkCredential(smtpAcct, smtpPwd)
         client.EnableSsl = smtpSSL
         client.Send(objMessage)
         LogMsg("Email has been sent to: " & sMailTo)
         SendMailEx = True
      Catch ex As Exception
         LogMsg("***** Error sending email to: " & sMailTo)
         LogMsg("---> Desc: " & ex.Message())
      End Try

   End Function

   'Send mails using preconfigured addresses
   Public Function doSendMail(ByVal sMailSubj As String, ByVal sMailBody As String, Optional ByVal sAttachment As String = "") As Boolean
      Dim smtpHost, smtpPort, smtpAcct, smtpPwd, smtpSSL As String
      Dim iTmp As Integer

      doSendMail = False

      'Check  mailto addr
      If g_iMailTo < 1 Then
         Exit Function
      End If

      ' message subject
      smtpHost = My.Settings.SmtpHost
      smtpAcct = My.Settings.SmtpUserID
      smtpPwd = My.Settings.SmtpPwd
      smtpPort = My.Settings.SmtpPort
      smtpSSL = My.Settings.SecuredSSL

      Try
         Dim mTo As New System.Net.Mail.MailAddress(g_asMailTo(0))
         Dim mFrom As New System.Net.Mail.MailAddress(gMailFrom)

         Dim objMessage As New System.Net.Mail.MailMessage(mFrom, mTo)

         objMessage.Subject = sMailSubj
         objMessage.Body = sMailBody

         For iTmp = 1 To g_iMailTo - 1
            objMessage.To.Add(g_asMailTo(iTmp))
         Next
         For iTmp = 0 To g_iCCMail - 1
            objMessage.CC.Add(g_asCCMail(iTmp))
         Next
         For iTmp = 0 To g_iBCCMail - 1
            objMessage.Bcc.Add(g_asBCCMail(iTmp))
         Next

         If Not String.IsNullOrEmpty(sAttachment) Then
            objMessage.Attachments.Add(New System.Net.Mail.Attachment(sAttachment))
         End If

         Dim client As New System.Net.Mail.SmtpClient(smtpHost, smtpPort)
         client.Credentials = New System.Net.NetworkCredential(smtpAcct, smtpPwd)
         client.EnableSsl = smtpSSL
         client.Send(objMessage)
         LogMsg("Email has been sent to: " & My.Settings.MailTo)
         doSendMail = True
      Catch ex As Exception
         LogMsg("***** Error sending email to: " & My.Settings.MailTo)
         LogMsg("---> Desc: " & ex.Message())
      End Try

   End Function

   'Sub SendMail()
   '   Dim Mail As New MailMessage()
   '   Mail.From = New MailAddress(txtFromAddress.Text, txtFromDisplayName.Text)
   '   Mail.To.Add(New MailAddress(txtSendAddress.Text, txtSendToDisplayName.Text))
   '   Mail.Subject = txtSubject.Text
   '   Dim plainView As AlternateView = AlternateView.CreateAlternateViewFromString(
   '       My.Resources.PlainTextFormatted, Nothing, "text/plain")
   '   Dim htmlView As AlternateView = AlternateView.CreateAlternateViewFromString(
   '       My.Resources.HtmlFormatted, Nothing, "text/html")
   '   Mail.AlternateViews.Add(plainView)
   '   Mail.AlternateViews.Add(htmlView)
   '   Mail.IsBodyHtml = chkUseHtmlBody.Checked
   '   Dim MailClient As New SmtpClient(txtHostServer.Text)
   '   MailClient.EnableSsl = chkEnableSsl.Checked
   '   If PortNumericUpDown.Value > 0 Then
   '      MailClient.Port = CInt(PortNumericUpDown.Value)
   '   End If
   '   txtResults.AppendText(String.Format("Connection Name: '{0}'",
   '                                       MailClient.ServicePoint.ConnectionName) &
   '                                   Environment.NewLine)
   '   MailClient.UseDefaultCredentials = True
   '   AddHandler MailClient.SendCompleted, AddressOf SendCompletedCallback
   '   Dim UserToken As String = "test message1"
   '   MailClient.Timeout = CInt(TimeOutNumericUpDown.Value)
   '   MailClient.SendAsync(Mail, UserToken)
   'End Sub
   'Private mailSent As Boolean = False
   'Private Sub SendCompletedCallback(ByVal sender As Object, ByVal e As AsyncCompletedEventArgs)
   '   Dim token As String = CStr(e.UserState)
   '   txtResults.AppendText(String.Format("Call back at {0} token [{1}]", Now, token) & Environment.NewLine)
   '   If e.Cancelled Then
   '      txtResults.AppendText(String.Format("[{0}] [{1}] Send canceled.", Now, token) & Environment.NewLine)
   '   End If
   '   If e.Error IsNot Nothing Then
   '      txtResults.AppendText(String.Format("[{0}] [{1} [{2}]] Send canceled.", Now, token, e.Error.ToString) & Environment.NewLine)
   '      GetInnerException(e.Error)
   '      My.Dialogs.ExceptionDialog(e.Error, "Failed see results.", "")
   '      ActiveControl = txtResults
   '   Else
   '      txtResults.AppendText("Message sent at '" & Now.ToString & "'" & Environment.NewLine)
   '      MessageBox.Show("Message sent!!!")
   '   End If
   '   mailSent = True
   'End Sub
   'Private Function GetInnerException(ByRef sender As Exception) As String
   '   Dim Data As String = String.Empty
   '   txtResults.AppendText(Environment.NewLine)
   '   Do While Not sender Is Nothing
   '      Data &= sender.ToString()
   '      txtResults.AppendText(String.Format("[{0}]", sender.Message) & Environment.NewLine)
   '      sender = sender.InnerException
   '   Loop
   '   Return Data
   'End Function
End Module