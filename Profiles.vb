Option Strict Off
Option Explicit On
Module Profiles

   Structure TProfile
      Dim CountyID As Integer
      Dim CountyCode As String
      Dim Medium As String
      Dim Steps As String
      Dim Format As String
      Dim Padding As Integer
      Dim Infile As String
      Dim Outfile As String
      Dim Zipfile As String
      Dim Frequency As String
      Dim Tape_Dir As String
      Dim State As String
      Dim LastRecDate As Integer
      Dim LastRecCount As Integer
      Dim LastBldDate As Integer
   End Structure

   Structure TCounty
      Dim CountyCode As String
      Dim Status As String
      Dim PrepType As String
      'Dim PrepFiles As String
      Dim ExCmd As String
      Dim LastFileDate As Integer
      Dim NumFiles As Integer
      <VBFixedArray(10)> Dim FileName() As String

      Public Sub Initialize()
         ReDim FileName(10)
      End Sub
   End Structure

   Public g_CtyProfile() As TProfile
   Public g_CtyList() As TCounty
   Public g_iFileCnt As Integer
   Public g_iCounties As Integer

   Public Sub getCountyInfo(ByRef sCnty() As String, ByRef iCnt As Integer)
      Dim iRet, iTmp As Integer
      Dim sql As String

      g_iCounties = 0
      ReDim g_CtyList(iCnt)

      For iTmp = 0 To iCnt - 1
         sql = "SELECT * FROM County WHERE (CountyCode='" & sCnty(iTmp) & "')"
         If Not openRS(sql) Then
            Exit Sub
         End If

         'Loop through record set and process everyone
         Try
            If Not dbRs.EOF Then
               g_CtyList(g_iCounties).CountyCode = dbRs.Fields("CountyCode").Value
               g_CtyList(g_iCounties).Status = dbRs.Fields("Status").Value
               g_CtyList(g_iCounties).LastFileDate = dbRs.Fields("LastFileDate").Value
               g_CtyList(g_iCounties).NumFiles = dbRs.Fields("NumFiles").Value
               g_CtyList(g_iCounties).PrepType = "" & dbRs.Fields("PrepType").Value
               g_CtyList(g_iCounties).ExCmd = "" & dbRs.Fields("ExCmd").Value
               g_CtyList(g_iCounties).Initialize()

               For iRet = 0 To g_CtyList(g_iCounties).NumFiles - 1
                  g_CtyList(g_iCounties).FileName(iRet) = dbRs.Fields("File" & iRet).Value
               Next
               g_iCounties = g_iCounties + 1
               g_CtyList(g_iCounties).CountyCode = ""
            End If
         Catch ex As Exception
            LogMsg("Error in getCountyInfo() for " & g_CtyList(g_iCounties).CountyCode)
            LogMsg("ErrMsg: " & ex.Message())
         End Try
         dbRs.Close()
      Next

      If g_CtyList(g_iCounties).CountyCode <> "" Then
         LogMsg("Error in getCountyInfo().  Please check: " & g_CtyList(g_iCounties).CountyCode)
      End If
   End Sub

   Public Sub getCounties()
      Dim iRet As Integer
      'Dim sTmp As String

      g_iCounties = 0
      ReDim g_CtyList(100)

      If Not openRS("SELECT * FROM County WHERE (Status='R')") Then
         Exit Sub
      End If

      'Loop through record set and process everyone
      Do While Not dbRs.EOF
         g_CtyList(g_iCounties).CountyCode = dbRs.Fields("CountyCode").Value
         g_CtyList(g_iCounties).Status = dbRs.Fields("Status").Value
         g_CtyList(g_iCounties).LastFileDate = dbRs.Fields("LastFileDate").Value
         g_CtyList(g_iCounties).NumFiles = dbRs.Fields("NumFiles").Value
         g_CtyList(g_iCounties).PrepType = "" & dbRs.Fields("PrepType").Value
         g_CtyList(g_iCounties).ExCmd = "" & dbRs.Fields("ExCmd").Value
         g_CtyList(g_iCounties).Initialize()

         For iRet = 0 To g_CtyList(g_iCounties).NumFiles - 1
            'sTmp = dbRs.Fields("File" & iRet).Value
            'If sTmp > " " Then
            '   g_CtyList(g_iCounties).FileName(iRet) = sTmp
            'End If
            g_CtyList(g_iCounties).FileName(iRet) = dbRs.Fields("File" & iRet).Value
         Next
         g_iCounties = g_iCounties + 1
         dbRs.MoveNext()
      Loop
      g_CtyList(g_iCounties).CountyCode = ""
      dbRs.Close()
   End Sub

   Public Sub UpdateCounty(ByRef sCnty As String)
      Dim iRet As Integer
      Dim sql As String

      sql = "SELECT * FROM County WHERE (CountyCode='" & sCnty & "')"
      If Not openRS(sql) Then
         Exit Sub
      End If

      'Loop through record set and process everyone
      iRet = 0
      Try
         If Not dbRs.EOF Then
            For iRet = 0 To g_iCounties - 1
               If g_CtyList(iRet).CountyCode = sCnty Then
                  dbRs.Fields("LastFileDate").Value = g_CtyList(iRet).LastFileDate
                  dbRs.Update()
                  Exit For
               End If
            Next
         End If
      Catch ex As Exception
         LogMsg("ERROR updating County table for " & sCnty)
      End Try

      'Close recordset
      dbRs.Close()
   End Sub

   Public Function LoadProfile(ByRef sCtyCode As String) As Integer
      Dim sql As String

      ReDim g_CtyProfile(10)

      sql = "SELECT * FROM CtyProfiles WHERE (CountyCode='" & sCtyCode & "')"
      If Not openRS(sql) Then
         LogMsg("***** ERROR opening CtyProfiles table for " & sCtyCode)
         LoadProfile = -1
         Exit Function
      End If

      g_iFileCnt = 0
      'Loop through record set and process everyone
      Do While Not dbRs.EOF
         g_CtyProfile(g_iFileCnt).CountyCode = sCtyCode
         g_CtyProfile(g_iFileCnt).CountyID = dbRs.Fields("CountyID").Value
         g_CtyProfile(g_iFileCnt).Medium = "" & dbRs.Fields("Medium").Value
         g_CtyProfile(g_iFileCnt).Steps = "" & dbRs.Fields("Steps").Value
         g_CtyProfile(g_iFileCnt).Format = "" & dbRs.Fields("Format").Value
         g_CtyProfile(g_iFileCnt).Padding = CInt("" & dbRs.Fields("Padding").Value)
         g_CtyProfile(g_iFileCnt).Infile = "" & dbRs.Fields("Infile").Value
         g_CtyProfile(g_iFileCnt).Outfile = "" & dbRs.Fields("Outfile").Value
         g_CtyProfile(g_iFileCnt).Zipfile = "" & dbRs.Fields("Zipfile").Value
         g_CtyProfile(g_iFileCnt).Frequency = "" & dbRs.Fields("Frequency").Value
         g_CtyProfile(g_iFileCnt).Tape_Dir = "" & dbRs.Fields("Tape_Dir").Value
         g_CtyProfile(g_iFileCnt).LastRecDate = Val("" & dbRs.Fields("LastRecDate").Value)
         g_CtyProfile(g_iFileCnt).LastRecCount = Val("" & dbRs.Fields("LastRecCount").Value)
         g_CtyProfile(g_iFileCnt).LastBldDate = Val("" & dbRs.Fields("LastBldDate").Value)
         g_iFileCnt = g_iFileCnt + 1
         dbRs.MoveNext()
      Loop

      LoadProfile = g_iFileCnt
   End Function

   Public Sub UpdateProfile(ByRef sCtyCode As String)
      Dim iRet As Integer
      Dim sql As String

      sql = "SELECT * FROM CtyProfiles WHERE (CountyCode='" & sCtyCode & "')"
      If Not openRS(sql) Then
         LogMsg("***** ERROR opening CtyProfiles table for " & sCtyCode)
         Exit Sub
      End If

      'Loop through record set and process everyone
      iRet = 0
      Do While Not dbRs.EOF
         dbRs.Fields("LastBldDate").Value = g_CtyProfile(iRet).LastBldDate
         dbRs.Fields("LastRecDate").Value = g_CtyProfile(iRet).LastRecDate
         dbRs.Fields("LastRecCount").Value = g_CtyProfile(iRet).LastRecCount
         dbRs.Fields("State").Value = g_CtyProfile(iRet).State
         iRet = iRet + 1
         dbRs.MoveNext()
      Loop

      dbRs.UpdateBatch()
      'Close recordset
      dbRs.Close()
   End Sub

   Public Sub closeDB()
      If Not dbRs Is Nothing Then
         If dbRs.State = ADODB.ObjectStateEnum.adStateOpen Then dbRs.Close()
      End If

      If Not dbConn Is Nothing Then
         If dbConn.State = ADODB.ObjectStateEnum.adStateOpen Then dbConn.Close()
      End If
   End Sub

   Public Function openDB() As Boolean
      Try
         If dbConn Is Nothing Then
            dbConn = New ADODB.Connection
            dbConn.ConnectionString = "Provider=" & gProvider
            dbConn.Open()
         ElseIf dbConn.State = ADODB.ObjectStateEnum.adStateClosed Then
            dbConn.ConnectionString = "Provider=" & gProvider
            dbConn.Open()
         End If
         openDB = True
      Catch ex As Exception
         LogMsg("***** Error connecting to Production DB: " & ex.Message())
         openDB = False
      End Try
   End Function

   Public Function openRS(ByRef sSqlCmd As String) As Boolean
      openRS = False

      If Not openDB() Then
         Exit Function
      End If

      Try
         If dbRs Is Nothing Then
            dbRs = New ADODB.Recordset
         ElseIf dbRs.State = ADODB.ObjectStateEnum.adStateOpen Then
            dbRs.Close()
         End If

         dbRs.CursorType = ADODB.CursorTypeEnum.adOpenKeyset
         dbRs.LockType = ADODB.LockTypeEnum.adLockOptimistic
         dbRs.Open(sSqlCmd, dbConn, , , ADODB.CommandTypeEnum.adCmdText)
         openRS = True
      Catch ex As Exception
         LogMsg("***** Error opening County table: " & ex.Message())
         openRS = False
      End Try
   End Function

   Public Function getPrepType(ByRef sCnty As String) As String
      Dim iRet As Integer
      Dim strRet As String

      strRet = ""
      For iRet = 0 To g_iCounties - 1
         If g_CtyList(iRet).CountyCode = sCnty Then
            strRet = g_CtyList(iRet).PrepType
            Exit For
         End If
      Next
      getPrepType = strRet
   End Function

   Public Function getExCmd(ByRef sCnty As String) As String
      Dim iRet As Integer
      Dim strRet As String

      strRet = ""
      For iRet = 0 To g_iCounties - 1
         If g_CtyList(iRet).CountyCode = sCnty Then
            strRet = g_CtyList(iRet).ExCmd
            Exit For
         End If
      Next
      getExCmd = strRet
   End Function

   Public Function getFile(ByRef sCnty As String, ByRef iFileNum As Integer) As String
      Dim iRet As Integer
      Dim strRet As String

      strRet = ""
      For iRet = 0 To g_iCounties - 1
         If g_CtyList(iRet).CountyCode = sCnty Then
            strRet = g_CtyList(iRet).FileName(iFileNum)
            Exit For
         End If
      Next
      getFile = strRet
   End Function

   Public Sub setProductState(ByRef sCnty As String, ByRef sState As String)
      Dim sql As String

      sql = "SELECT * FROM Products WHERE (CountyCode='" & sCnty & "')"
      If Not openRS(sql) Then
         LogMsg("***** ERROR updating Products table for " & sCnty)
         Exit Sub
      End If

      'Loop through record set and process everyone
      Try
         While Not dbRs.EOF
            dbRs.Fields("State").Value = sState
            dbRs.Update()
            dbRs.MoveNext()
         End While
      Catch ex As Exception
         LogMsg("***** Exception error updating Products table: " & ex.Message)
      End Try

      'Close recordset
      dbRs.Close()
   End Sub
End Module