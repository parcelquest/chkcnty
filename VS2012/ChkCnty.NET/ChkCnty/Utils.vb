Option Strict Off
Option Explicit On
Imports System.IO
Imports VB = Microsoft.VisualBasic
Imports System.Runtime.InteropServices

Module Utils

   Public Structure ADDR1_DEF
      Dim strNum As String
      Dim strSub As String
      Dim strDir As String
      Dim strName As String
      Dim strSfx As String
      Dim strUnit As String
   End Structure

   Public Structure ADDR2_DEF
      Dim sCity As String
      Dim sState As String
      Dim sZip As String
      Dim sZip4 As String
   End Structure

   Private Const M_STRNUM_OFF As Integer = 1
   Private Const M_STRSUB_OFF As Integer = 8
   Private Const M_STRDIR_OFF As Integer = 11
   Private Const M_STRNAM_OFF As Integer = 13
   Private Const M_STRSFX_OFF As Integer = 37
   Private Const M_STRUNIT_OFF As Integer = 42
   Private Const M_CITY_OFF As Integer = 1
   Private Const M_STATE_OFF As Integer = 18
   Private Const M_ZIP_OFF As Integer = 20
   Private Const M_ZIP4_OFF As Integer = 25

   Public g_logFile As String
   Private sLogMsg As String


   Public Function readTextFile(ByRef fileName As String) As String
      Dim strBuf As String
      Dim strTmp As String
      Dim fd As Integer

      fd = FreeFile()
      FileOpen(fd, fileName, OpenMode.Input, OpenAccess.Read, OpenShare.LockWrite)
      strBuf = ""

      Do While Not EOF(fd)
         strTmp = LineInput(fd)
         strBuf = strBuf & strTmp
      Loop

      FileClose(fd)
      readTextFile = strBuf
   End Function

   Public Function LastLogMsg() As String
      LastLogMsg = sLogMsg
   End Function

   Public Sub LogMsg(ByVal strMsg As String)
      Dim ff As Integer

      On Error GoTo ErrorHandler
      ff = FreeFile()

      sLogMsg = Now & vbTab & strMsg
      FileOpen(ff, g_logFile, OpenMode.Append)
      PrintLine(ff, sLogMsg)
      FileClose(ff)

      Exit Sub

ErrorHandler:
      'MsgBox "Please check for diskspace on " & g_logFile
   End Sub

   Public Sub LogMsg0(ByVal strMsg As String)
      Dim ff As Integer

      On Error GoTo ErrorHandler
      ff = FreeFile()

      sLogMsg = strMsg
      FileOpen(ff, g_logFile, OpenMode.Append)
      PrintLine(ff, sLogMsg)

      FileClose(ff)

ErrorHandler:
   End Sub

   Public Function FormatData(ByRef Length As Integer, ByRef DataType As String, ByRef Field As String) As String
      Dim fldLen As Integer
      Dim fldData As String
      Dim strTmp As String

      If Field = "" Then
         fldLen = 0
         fldData = ""
      Else
         fldData = Trim(Field)
         fldLen = Len(fldData)
      End If

      FormatData = ""
      Select Case DataType
         Case "CHARACTER"
            If fldLen >= Length Then
               FormatData = Left(fldData, Length)
            Else
               FormatData = IIf(fldLen = 0, New String(" ", Length), fldData & New String(" ", Length - fldLen))
            End If
         Case "NUMERIC"
            FormatData = IIf(fldLen = 0, New String("0", Length), New String("0", Length - fldLen) & fldData)
         Case "DATE"
            FormatData = IIf(fldLen = 0, New String(" ", 8), VB.Format(fldData, "YYYYMMDD"))
         Case "DATE8"
            strTmp = IIf(fldLen = 0, New String(" ", 8), VB.Format(fldData, "MMDDYYYY"))
            If strTmp = "01011900" Or strTmp = "01011800" Then
               FormatData = New String(" ", 8)
            Else
               FormatData = strTmp
            End If
         Case "DATE6"
            'strTmp = IIf(IsNull(Field), String$(6, " "), Format$(fldData, "MMYYYY"))
            If fldLen = 0 Or fldLen > 6 Then
               strTmp = IIf(fldLen = 0, New String(" ", 6), VB.Format(fldData, "MMYYYY"))
            Else
               strTmp = fldData
            End If
            If strTmp = "011900" Or strTmp = "011800" Then
               strTmp = New String(" ", 6)
            End If
            FormatData = strTmp
      End Select
   End Function

   Public Function FormatData1(ByRef Length As Integer, ByRef DataType As String, ByRef Field As String) As String
      Dim fldLen As Integer
      Dim fldData As String
      Dim strTmp, strTmp1 As String
      Dim lTmp As Integer

      On Error GoTo FormatData1_Error

      If fldLen = 0 Then
         fldLen = 0
         fldData = ""
      Else
         fldData = Trim(Field)
         fldLen = Len(fldData)
      End If

      Select Case DataType
         Case "C"
            If fldLen >= Length Then
               strTmp = Left(fldData, Length)
            Else
               strTmp = IIf(fldLen = 0, Space(Length), fldData & Space(Length - fldLen))
            End If
         Case "D"
            strTmp = IIf(fldLen = 0, Space(8), VB.Format(fldData, "YYYYMMDD"))
         Case "D1" 'YYYYMMDD -> MMYY
            If fldLen <> 8 Then
               strTmp = New String("0", 4)
            Else
               strTmp = Mid(fldData, 5, 2) & Mid(fldData, 3, 2)
            End If
         Case "D2" 'YYYYMMDD -> MMDDYY
            If fldLen <> 8 Then
               strTmp = New String("0", 6)
            Else
               strTmp = Mid(fldData, 5, 4) & Mid(fldData, 3, 2)
            End If
         Case "D3" 'MMDDYYYY -> MMYY
            If fldLen <> 8 Then
               strTmp = Space(4)
            Else
               strTmp = Mid(fldData, 5, 2) & Mid(fldData, 3, 2)
            End If
         Case "D4" 'MMDDYYYY -> MMDDYY
            If fldLen <> 8 Then
               strTmp = Space(6)
            Else
               strTmp = Left(fldData, 4) & Right(fldData, 2)
            End If
         Case "D6", "DATE6"
            If fldLen = 0 Or fldLen > 6 Then
               strTmp = IIf(fldLen = 0, Space(6), VB.Format(fldData, "MMYYYY"))
            Else
               strTmp = fldData
            End If
            If strTmp = "011900" Or strTmp = "011800" Then
               strTmp = Space(6)
            End If
         Case "D8"
            strTmp = IIf(fldLen = 0, Space(8), VB.Format(fldData, "MMDDYYYY"))
            If strTmp = "01011900" Or strTmp = "01011800" Then
               strTmp = Space(8)
            Else
               strTmp = strTmp
            End If
         Case "L"
            If fldLen >= Length Then
               strTmp = Left(fldData, Length)
            Else
               strTmp = IIf(fldLen = 0, Space(Length), fldData & Space(Length - fldLen))
            End If
         Case "L0"
            If fldLen >= Length Then
               strTmp = Left(fldData, Length)
            Else
               strTmp = IIf(fldLen = 0, New String("0", Length), fldData & New String("0", Length - fldLen))
            End If
         Case "R"
            If fldLen >= Length Then
               strTmp = Right(fldData, Length)
            Else
               strTmp = IIf(fldLen = 0, Space(Length), Space(Length - fldLen) & fldData)
            End If
         Case "R0"
            If fldLen >= Length Then
               strTmp = Right(fldData, Length)
            Else
               strTmp = IIf(fldLen = 0, New String("0", Length), New String("0", Length - fldLen) & fldData)
            End If
         Case "Z"
            If fldLen > Length Then
               strTmp = Right(fldData, Length)
            Else
               strTmp = IIf(fldLen = 0, New String("0", Length), New String("0", Length - fldLen) & fldData)
            End If
         Case "Z1"
            If fldLen > 0 Then
               lTmp = 10 * Val(Field)
               fldData = CStr(lTmp)
               fldLen = Len(fldData)
            End If
            strTmp = IIf(fldLen = 0, New String("0", Length), New String("0", Length - fldLen) & fldData)
         Case "Z2"
            If fldLen > 1 Then
               lTmp = 100 * Val(Field)
               fldData = CStr(lTmp)
               fldLen = Len(fldData)
            End If
            strTmp = IIf(fldLen = 0, New String("0", Length), New String("0", Length - fldLen) & fldData)
         Case "ZB"
            If fldLen > 1 Then
               lTmp = Val(Field)
               If lTmp = 0 Then
                  fldData = " "
                  fldLen = 1
               End If
            End If
            strTmp = IIf(fldLen = 0, Space(Length), Space(Length - fldLen) & fldData)
         Case "ZR"
            If fldLen >= Length Then
               strTmp = Right(fldData, Length)
            Else
               strTmp = IIf(fldLen = 0, New String("0", Length), New String("0", Length - fldLen) & fldData)
            End If
         Case "F" 'Filler
            strTmp = New String("0", Length)
         Case Else 'Filler
            strTmp = New String("*", Length)
      End Select
      FormatData1 = strTmp
      Exit Function

FormatData1_Error:
      MsgBox("Error: " & Err.Description)
      FormatData1 = fldData
   End Function

   Public Function FormatDisplay(ByRef Length As Integer, ByRef DataType As String, ByRef Field As String) As String
      Dim fldLen As Integer
      Dim fldData As String
      Dim strTmp, strTmp1 As String
      Dim lTmp As Integer
      Dim dTmp As Double

      On Error GoTo FormatDisplay_Error

      If Field = "" Then
         fldLen = 0
         fldData = ""
      Else
         fldData = Trim(Field)
         fldLen = Len(fldData)
         If (fldLen > 4) And (fldData = New String("9", fldLen)) Then
            fldData = ""
            fldLen = 0
         End If
      End If

      Select Case DataType
         Case "$"
            If fldLen = 0 Then
               strTmp = Space(Length)
            Else
               strTmp1 = VB.Format(fldData, "$##,##")
               strTmp = Space(Length - Len(strTmp1)) & strTmp1
            End If
         Case "$100"
            If fldLen = 0 Then
               strTmp = Space(Length)
            Else
               lTmp = Val(fldData)
               dTmp = lTmp / 100
               strTmp1 = VB.Format(dTmp, "$##,##.00")
               strTmp = Space(Length - Len(strTmp1)) & strTmp1
            End If
         Case "A" 'Format acreage
            If fldLen = 0 Then
               strTmp = Space(Length)
            Else
               If fldLen > 3 Then
                  strTmp1 = "." & Right(fldData, 3)
                  fldData = Left(fldData, Len(fldData) - 3)
                  fldLen = Len(fldData)
                  If fldLen > 3 Then
                     strTmp1 = Left(fldData, fldLen - 3) & "," & Right(fldData, 3) & strTmp1
                  Else
                     strTmp1 = fldData & strTmp1
                  End If
               Else
                  strTmp1 = "0." & fldData & New String("0", 3 - fldLen)
               End If
               strTmp = Space(Length - Len(strTmp1)) & strTmp1
            End If
         Case "D" 'YYYYMMDD -> MM/DD/YYYY
            If fldLen <> 8 Then
               strTmp = Space(10)
            Else
               strTmp = Mid(fldData, 5, 2) & "/" & Mid(fldData, 7, 2) & "/" & Left(fldData, 4)
            End If
         Case "N"
            If fldLen = 0 Then
               strTmp = Space(Length)
            Else
               strTmp1 = VB.Format(fldData, "##,##")
               strTmp = Space(Length - Len(strTmp1)) & strTmp1
            End If

         Case Else 'Filler
            strTmp = New String("*", Length)
      End Select
      FormatDisplay = strTmp
      Exit Function

FormatDisplay_Error:
      MsgBox("Error: " & Err.Description)
      FormatDisplay = fldData
   End Function

   Public Function FormatMAddr1(ByRef Length As Integer, ByRef DataType As String, ByRef Field As ADDR1_DEF) As String
      Dim strTmp, strTmp1 As String

      On Error GoTo FormatMAddr1_Err

      If Len(Field.strName) = 0 Then
         strTmp = Space(Length)
      Else
         strTmp1 = ""
         If Field.strNum <> "" Then
            strTmp1 = Field.strNum & " "
         End If
         If Field.strDir <> "" Then
            strTmp1 = strTmp1 & Field.strDir & " "
         End If

         strTmp1 = strTmp1 & Field.strName & " "

         If Field.strSfx <> "" Then
            strTmp1 = strTmp1 & Field.strSfx & " "
         End If
         If Field.strUnit <> "" Then
            strTmp1 = strTmp1 & "#" & Field.strUnit
         End If

         strTmp = strTmp1 & Space(Length - Len(strTmp1))
      End If

      FormatMAddr1 = strTmp
      Exit Function

FormatMAddr1_Err:
      FormatMAddr1 = Space(Length)
   End Function

   Public Function FormatMAddr2(ByRef Length As Integer, ByRef DataType As String, ByRef Field As ADDR2_DEF) As String
      Dim strTmp, strTmp1 As String
      Dim lZip As Integer

      On Error GoTo FormatMAddr2_Err

      If Len(Field.sCity) = 0 Then
         strTmp = Space(Length)
      Else
         strTmp1 = Field.sCity & " "
         If Field.sState <> "" Then
            strTmp1 = strTmp1 & Field.sState & " "
         End If
         lZip = Val(Field.sZip)
         If lZip > 0 Then
            strTmp1 = strTmp1 & Field.sZip

            lZip = Val(Field.sZip4)
            If lZip > 0 Then
               strTmp1 = strTmp1 & "-" & Field.sZip4
            End If
         End If

         strTmp = strTmp1 & Space(Length - Len(strTmp1))
      End If

      FormatMAddr2 = strTmp
      Exit Function

FormatMAddr2_Err:
      FormatMAddr2 = Space(Length)
   End Function

   Public Sub dispMsg(ByRef strMsg As String, ByRef bLogOnly As Boolean)
      If bLogOnly Then
         LogMsg(strMsg)
      Else
         MsgBox(strMsg)
      End If
   End Sub

   '<DllImport("KERNEL32.DLL", EntryPoint:="OpenProcess", SetLastError:=True,
   '    CharSet:=CharSet.Unicode, ExactSpelling:=True,
   '    CallingConvention:=CallingConvention.StdCall)>
   'Public Function OpenProcess(ByVal access As Integer, ByVal inherit As Boolean, ByVal processId As Integer) As Long
   'End Function
   '<DllImport("KERNEL32.DLL", EntryPoint:="WaitForSingleObject", SetLastError:=True,
   '    CharSet:=CharSet.Unicode, ExactSpelling:=True,
   '    CallingConvention:=CallingConvention.StdCall)>
   'Public Function WaitForSingleObject(ByVal access As Long, ByVal lTime As Long) As Long
   'End Function
   '<DllImport("KERNEL32.DLL", EntryPoint:="CloseHandle", SetLastError:=True,
   '    CharSet:=CharSet.Unicode, ExactSpelling:=True,
   '    CallingConvention:=CallingConvention.StdCall)>
   'Public Function CloseHandle(ByVal access As Long) As Long
   'End Function

   'Public Sub WaitOnProgram(ByVal hInst As Long, Optional ByVal lSecs As Long = 3600000)
   '   Dim hProc As Long
   '   Dim iRet As Long
   '   Dim bRet As Boolean

   '   'The access right in this function must be called differently on 64-bit system.
   '   hProc = OpenProcess(2035711, False, hInst)
   '   If hProc <> vbNull Then
   '      iRet = WaitForSingleObject(hProc, lSecs)      'Give it 1 hour to run
   '      bRet = CloseHandle(hProc)
   '   End If
   'End Sub

   Public Function removeBlank(ByRef sStr As String) As String
      removeBlank = sStr
   End Function

   Public Function removeBlankAt(ByRef sStr As String, ByRef iPos As Integer) As String
      Dim sTmp As String

      If Mid(sStr, iPos, 1) = " " Then
         sTmp = Left(sStr, iPos - 1) & Mid(sStr, iPos + 1)
      Else
         sTmp = sStr
      End If
      removeBlankAt = RTrim(sTmp)
   End Function

   Public Function removeLeadingZero(ByRef sStr As String) As String
      Dim biTmp As Double

      biTmp = CDbl("" & sStr)
      removeLeadingZero = CStr(biTmp)
   End Function

   Public Function stripQuote(ByRef sStr As String) As String
      Dim sTmp As String

      If Left(sStr, 1) = Chr(34) Then
         sTmp = Mid(sStr, 2, Len(sStr) - 2)
      Else
         sTmp = sStr
      End If

      stripQuote = sTmp
   End Function

   Public Function getPath(ByRef sPathName As String) As String
      Dim iPos As Integer
      Dim sPath As String

      On Error GoTo getPath_Err

      sPath = ""
      If sPathName <> "" Then
         iPos = InStrRev(sPathName, "\")
         If iPos > 1 Then
            sPath = Left(sPathName, iPos)
         End If
      End If

      getPath = sPath
      Exit Function
getPath_Err:
      getPath = ""
   End Function

   'Get modified time
   Public Function getFileMTime(ByVal strFilename As String) As String
      Dim strTmp As String

      Try
         Dim dt As DateTime = File.GetLastWriteTime(strFilename)
         strTmp = Format(dt, "yyyyMMdd")
      Catch ex As Exception
         strTmp = ""
      End Try

      getFileMTime = strTmp
   End Function

   'Return 0 if nothing change
   Public Sub ShellandWait(ByVal ProcessPath As String)
      Dim objProcess As System.Diagnostics.Process
      Try
         objProcess = New System.Diagnostics.Process()
         objProcess.StartInfo.FileName = ProcessPath
         objProcess.StartInfo.WindowStyle = ProcessWindowStyle.Normal
         objProcess.Start()

         'Wait until the process passes back an exit code 
         objProcess.WaitForExit()

         'Free resources associated with this process
         objProcess.Close()
      Catch ex As Exception
         LogMsg("Could not start process: " & ProcessPath)
         LogMsg("Error: " & ex.Message())
      End Try
   End Sub

   'This function return error code from sAppName
   Public Function doShellNWait(ByRef sAppName As String, ByRef sParams As String) As Integer
      Dim iRet As Integer
      Dim newProc As Diagnostics.Process

      LogMsg(sAppName & " " & sParams)
      iRet = -1
      Try
         newProc = Diagnostics.Process.Start(sAppName, sParams)
         newProc.WaitForExit()
         If newProc.HasExited Then
            iRet = newProc.ExitCode
         End If
      Catch ex As Exception
         LogMsg("***** Error executing above command: " & ex.Message())
      End Try

      doShellNWait = iRet
   End Function

   'Check for leap year
   Public Function isLeapYear(ByRef iYear As Integer) As Boolean
      If (iYear Mod 4) = 0 Then
         isLeapYear = True
      Else
         isLeapYear = False
      End If
   End Function

   'Return number of days in a given month/year
   Public Function getDaysInMonth(ByRef iMonth As Integer, ByRef iYear As Integer) As Integer
      Dim iDay As Integer

      Select Case iMonth
         Case 1, 3, 5, 7, 8, 10, 12
            iDay = 31
         Case 2
            If isLeapYear(iYear) Then
               iDay = 29
            Else
               iDay = 28
            End If
         Case 4, 6, 9, 11
            iDay = 30
      End Select
      getDaysInMonth = iDay
   End Function

   'Input date must be in format as MM/DD/YYYY
   'Strictly compare with current date.  Return false if it is a future date
   Public Function isValidMMDDYYYY(ByRef sDate As String) As Boolean
      Dim iYY As Short
      Dim iMM As Short
      Dim iDD As Short
      Dim iCurYear As Short
      Dim iCurMonth As Short
      Dim iCurDay As Short
      Dim iDay As Short

      isValidMMDDYYYY = True
      If Mid(sDate, 3, 1) = "/" And Mid(sDate, 6, 1) = "/" And Len(sDate) = 10 Then
         iMM = CShort(Left(sDate, 2))
         iDD = CShort(Mid(sDate, 4, 2))
         iYY = CShort(Right(sDate, 4))
         iCurYear = CShort(VB.Format(Now, "YYYY"))
         iCurMonth = CShort(VB.Format(Now, "MM"))
         iCurDay = CShort(VB.Format(Now, "DD"))

         'Check current year and month
         If iYY > iCurYear Or iYY < 1500 Or iMM > 12 Or iMM < 1 Then
            isValidMMDDYYYY = False
         ElseIf iYY = iCurYear And iMM > iCurMonth Then
            isValidMMDDYYYY = False
         ElseIf iYY = iCurYear And iMM = iCurMonth And iDD > iCurDay Then
            isValidMMDDYYYY = False
         Else
            'Check day
            iDay = getDaysInMonth(iMM, iYY)
            If iDD > iDay Or iDD < 1 Then
               isValidMMDDYYYY = False
            End If
         End If
      Else
         isValidMMDDYYYY = False
      End If
   End Function

   'Relax version - just check for valid date.  Don't care about greater than current date
   Public Function isValidMMDDYYYY_1(ByRef sDate As String) As Boolean
      Dim iYY As Short
      Dim iMM As Short
      Dim iDD As Short
      Dim iDay As Short

      isValidMMDDYYYY_1 = True
      If Mid(sDate, 3, 1) = "/" And Mid(sDate, 6, 1) = "/" And Len(sDate) = 10 Then
         iMM = CShort(Left(sDate, 2))
         iDD = CShort(Mid(sDate, 4, 2))
         iYY = CShort(Right(sDate, 4))

         'Check current year and month
         If iYY > 2999 Or iYY < 1500 Or iMM > 12 Or iMM < 1 Then
            isValidMMDDYYYY_1 = False
         Else
            'Check day
            iDay = getDaysInMonth(iMM, iYY)
            If iDD > iDay Or iDD < 1 Then
               isValidMMDDYYYY_1 = False
            End If
         End If
      Else
         isValidMMDDYYYY_1 = False
      End If
   End Function

   Public Sub Wait4Me(ByRef lSeconds As Integer)
      Dim lTmp As Integer
      lTmp = VB.Timer()
      Do While VB.Timer() < (lSeconds + lTmp)
         System.Windows.Forms.Application.DoEvents()
      Loop
   End Sub

   'This function verifies whether file size has been changed in specified time.
   'Return 0 if size changed, -1 if error, actual file size if nothing has been changed
   Public Function chkFileChange(ByRef sFile As String, Optional ByVal iSec As Integer = 5) As Integer
      Dim lRet, lFileSize As Long

      If Dir(sFile) = "" Then
         lRet = 0
      Else
         Try
            'Wait for 5 second then check file size again
            lFileSize = My.Computer.FileSystem.GetFileInfo(sFile).Length()
            Wait4Me(iSec)

            'Recheck filesize
            lRet = My.Computer.FileSystem.GetFileInfo(sFile).Length()
            If lFileSize <> lRet Then
               lRet = 0
            End If
         Catch ex As Exception
            LogMsg("Error in ChkFileChange(): " & ex.Message())
            lRet = -1
         End Try
      End If

      chkFileChange = lRet
   End Function

   'Return 0 if nothing change, -1 if error, 1 if not avail, 2 if changed.
   Public Function validateFile(ByRef sFile As String, Optional ByVal iSec As Integer = 5) As Integer
      Dim lRet, lFileSize As Long
      Dim iRet As Integer

      If Dir(sFile) = "" Then
         iRet = 1
      Else
         Try
            'Wait for 5 second then check file size again
            lFileSize = My.Computer.FileSystem.GetFileInfo(sFile).Length
            Wait4Me(iSec)

            'Recheck filesize
            lRet = My.Computer.FileSystem.GetFileInfo(sFile).Length

            If lFileSize = 0 Or lFileSize <> lRet Then
               iRet = 2
            Else
               iRet = 0
            End If
         Catch ex As Exception
            LogMsg("Error in validateFile: " & ex.Message())
            iRet = -1
         End Try
      End If
      validateFile = iRet
   End Function

End Module