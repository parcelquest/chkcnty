Option Strict Off
Option Explicit On
Imports VB = Microsoft.VisualBasic
Module modMain
   'To automate the process, run CHKCNTY ALL

   Public Const gcTaxRate As Double = 1.1

   Public gProvider As String
   Public gInputDir As String
   Public gOutputDir As String
   Public dbConn As ADODB.Connection
   Public dbRs As ADODB.Recordset
   Public g_bMailToError As Boolean
   Public g_sMachine As String

   Dim iNumCounties As Integer

   Public Sub Main()
      Dim gInitialized As Boolean
      Dim iCnt, iRet, iZipCnt, hInst As Integer
      Dim sCounty(58) As String
      Dim sPrepType, sPostType As String
      Dim sFile, sToken, sTmp, sCmd As String

      'We don't want multiple copy to run at the same time
      If (UBound(Diagnostics.Process.GetProcessesByName(Diagnostics.Process.GetCurrentProcess.ProcessName)) > 0) Then Exit Sub

      gInitialized = InitSettings() 'Initialize settings
      If Not gInitialized Then
         LogMsg("***** Error initializing ChkCnty: " & Err.Description)
         g_bMailToError = True
         GoTo Exit_Main
      End If

      'Check command line
      sCmd = VB.Command()
      If sCmd = "" Then
         Console.WriteLine("Usage: ChkCnty <Product Code|ALL>")
         Exit Sub
      End If

      LogMsg("ChkCnty " & My.Application.Info.Version.Major & "." & My.Application.Info.Version.Minor & "." & My.Application.Info.Version.Revision)

      'Check for ALL
      If sCmd = "ALL" Then
         iRet = getCntyList(sCmd)
         If iRet = 0 Then
            LogMsg("No county available to process!")
            g_bMailToError = True
            Exit Sub
         End If
      End If

      ' Parse command line
      iCnt = 0
      sToken = GetToken(sCmd, " ")
      Do While sToken <> ""
         sCounty(iCnt) = sToken
         iCnt = iCnt + 1
         sToken = GetToken(sEmpty, " ")
      Loop

      If g_iCounties = 0 Then
         Call getCountyInfo(sCounty, iCnt)
      End If
      LogMsg("Processing " & sCmd)

      'Preprocess
      For iCntyIdx As Integer = 0 To iCnt - 1
         'E=Extract,F=Filter,M=Merge,T=Filter then Merge,Z=Unzip then Extract
         sPrepType = getPrepType(sCounty(iCntyIdx))
         sPostType = ""

         'Unzip data before execute external command
         If sPrepType = "Z" Then
            'Unzip
            For iZipCnt = 0 To g_CtyList(iCntyIdx).NumFiles - 1
               If InStr(2, UCase(g_CtyList(iCntyIdx).FileName(iZipCnt)), ".ZIP") Then
                  sFile = gInputDir & "\" & sCounty(iCntyIdx) & "\" & g_CtyList(iCntyIdx).FileName(iZipCnt)
                  iRet = validateFile(sFile, My.Settings.TimeDelay)
                  If iRet = 0 Then
                     LogMsg("Unzip " & sFile)
                     iRet = UnzipFile1(sFile, gInputDir & "\" & sCounty(iCntyIdx))
                     If iRet = 0 Then
                        sPrepType = "X"
                        'Set file date
                        setLastFileDate(iCntyIdx, sFile)
                     Else
                        LogMsg("***** Fail to unzip " & sFile)
                        gMailSubject = "ChkCnty - Fail to unzip: " & sFile
                        gMailBody = "Please review log file for more detail."
                        doSendMail(gMailSubject, gMailBody, g_logFile)
                        sCounty(iCntyIdx) = "1" & sCounty(iCntyIdx)
                        Exit For
                     End If
                  Else
                     LogMsg("** Z:" & iRet & " - " & sCounty(iCntyIdx) & " (" & sFile & ") data is not ready for processing yet.")
                  End If
               ElseIf InStr(2, UCase(g_CtyList(iCntyIdx).FileName(iZipCnt)), ".GZ") Then
                  sFile = gInputDir & "\" & sCounty(iCntyIdx) & "\" & g_CtyList(iCntyIdx).FileName(iZipCnt)
                  iRet = validateFile(sFile, My.Settings.TimeDelay)
                  If iRet = 0 Then
                     sCmd = " -df " & sFile
                     iRet = doShellNWait(My.Settings.GUnzip, sCmd)
                     If iRet <> 0 Then
                        LogMsg("***** Fail to unzip " & sFile)
                        gMailSubject = "ChkCnty - Fail to unzip: " & sFile
                        gMailBody = "Please review log file for more detail."
                        doSendMail(gMailSubject, gMailBody, g_logFile)
                        sCounty(iCntyIdx) = "1" & sCounty(iCntyIdx)
                        sPrepType = ""
                        Exit For
                     Else
                        sPrepType = "X"
                        'Set file date
                        setLastFileDate(iCntyIdx, sFile)
                     End If
                  Else
                     LogMsg("** Z:" & iRet & " - " & sCounty(iCntyIdx) & " (" & sFile & ") data is not ready for processing yet.")
                  End If
               End If
            Next
         End If

         'Execute command
         If sPrepType = "X" Then
            iRet = 0
            sFile = "???"
            If g_CtyList(iCntyIdx).PrepType = "X" Then
               'Validating input file
               For iTmp1 As Integer = 1 To g_CtyList(iCntyIdx).NumFiles
                  sFile = gInputDir & "\" & sCounty(iCntyIdx) & "\" & g_CtyList(iCntyIdx).FileName(iTmp1 - 1)
                  iRet = validateFile(sFile, My.Settings.TimeDelay)
                  If iRet <> 0 Then
                     Exit For
                  End If
               Next
            End If

            If iRet = 0 Then
               'Assuming command has full path
               sCmd = getExCmd(sCounty(iCntyIdx))
               LogMsg("Launch command: " & sCmd)
               Try
                  hInst = Shell(sCmd, AppWinStyle.MinimizedNoFocus, True, 9000000)   'wait for 150 mins
                  If hInst > 0 Then
                     LogMsg("***** Process timeout after 15 mins <" & sCmd & ">")
                     gMailSubject = "ChkCnty - Sorting take more than 15 mins.  program abort!"
                     g_bMailToError = True
                     GoTo Exit_Main
                  End If
               Catch ex As Exception
                  LogMsg("***** Unable to create process to unzip: " & sCmd)
                  LogMsg("ErrMsg: " & ex.Message)
                  gMailSubject = "ChkCnty - Fail to execute command: " & sCmd
                  g_bMailToError = True
                  GoTo Exit_Main
               End Try

               LogMsg("Launch complete.")

               'Set last modified time of input file
               Call setLastMTime(iCntyIdx)

               'Update County table
               Call UpdateCounty(sCounty(iCntyIdx))
               'Signal to skip  other processing steps
               sCounty(iCntyIdx) = "0" & sCounty(iCntyIdx)
            Else
               LogMsg("** X:" & iRet & " - " & sCounty(iCntyIdx) & " (" & sFile & ") data is not ready for processing yet.")
               sCounty(iCntyIdx) = "1" & sCounty(iCntyIdx)
            End If
         End If

         'Rename the files back to original
         If sPostType = "R" Then
            sTmp = gInputDir & "\" & sCounty(iCntyIdx) & "\"
            For iRet = 0 To g_iFileCnt - 1
               If Dir(sTmp & g_CtyProfile(iRet).Infile) <> "" Then
                  Kill(sTmp & g_CtyProfile(iRet).Infile)
               End If
               If Dir(sTmp & g_CtyProfile(iRet).Outfile) <> "" Then
                  Rename(sTmp & g_CtyProfile(iRet).Outfile, sTmp & g_CtyProfile(iRet).Infile)
               End If
            Next
         End If
Next_County:
      Next

      LogMsg("Processing complete !!!")

Exit_Main:
      If g_bMailToError Then
         If gMailBody = "" Then
            gMailBody = "***** Error in ChkCnty.  Please review log file for more detail."
         End If

         If gMailSubject = "" Then
            gMailSubject = "Exception occurs in CHKCNTY.EXE"
         End If

         'On Error Resume Next
         doSendMail(gMailSubject, gMailBody, g_logFile)
      End If
   End Sub

   Private Function InitSettings() As Boolean
      Dim sTmp, sDelim As String

      InitSettings = True
      g_bMailToError = False
      g_sMachine = System.Environment.MachineName

      'Setup mail info
      sTmp = My.Settings.MailTo
      If InStr(sTmp, ";") > 1 Then
         sDelim = ";"
      Else
         sDelim = ","
      End If
      g_iMailTo = ParseStr(sTmp, sDelim, g_asMailTo)

      sTmp = My.Settings.MailCC
      If sTmp > "" Then
         g_iCCMail = ParseStr(sTmp, sDelim, g_asCCMail)
      End If
      gMailFrom = My.Settings.MailFrom
      gMailName = "Production ChkCnty"

      'Setup DB info
      gProvider = My.Settings.SqlConn

      'Setup processing folders
      gInputDir = Replace(My.Settings.InputDir, "[Machine]", g_sMachine)
      gOutputDir = Replace(My.Settings.OutputDir, "[Machine]", g_sMachine)
      sTmp = My.Settings.LogDir & "\" & VB.Format(Now, "yyyyMMdd") & ".log"
      g_logFile = Replace(sTmp, "[Machine]", g_sMachine)

   End Function

   'This function will look for list of county then check to see if new files available.  
   Private Function getCntyList(ByRef sList As String) As Integer
      Dim iRet, iCnt As Integer

      'Get list from CtyProfiles
      Call getCounties()

      'Check for available data
      Call checkNewData()

      'Resort the list and remove empty cell
      iRet = 0
      sList = ""
      For iCnt = 0 To g_iCounties - 1
         If g_CtyList(iCnt).Status = "Y" Then
            If iRet < iCnt Then
               g_CtyList(iRet) = g_CtyList(iCnt)
            End If
            sList = sList & g_CtyList(iCnt).CountyCode & " "
            g_CtyList(iCnt).Status = ""
            iRet = iRet + 1
         End If
      Next
      g_iCounties = iRet
      getCntyList = iRet
   End Function

   'Check for counties with updated files, then set status='Y'
   Private Sub checkNewData()
      Dim iCnt, iNdx, iTime As Integer
      Dim sTmp, sTime As String
      Dim lSize As Long

      On Error GoTo checkNewData_Error

      For iCnt = 0 To g_iCounties - 1
         For iNdx = 0 To g_CtyList(iCnt).NumFiles - 1
            sTmp = gInputDir & "\" & g_CtyList(iCnt).CountyCode & "\" & g_CtyList(iCnt).FileName(iNdx)
            If Dir(sTmp) = "" Then
               Exit For
            Else
               sTime = getFileMTime(sTmp)
               iTime = sTime
               If iTime <= g_CtyList(iCnt).LastFileDate Then
                  'Debug info
                  If iNdx > 0 Then
                     LogMsg(sTmp & ": current file date=" & iTime & ", last file date=" & g_CtyList(iCnt).LastFileDate)
                  End If
                  Exit For
               End If

               'Check file size.  If empty, break
               lSize = chkFileChange(sTmp, My.Settings.TimeDelay)
               If (lSize = 0) Or (lSize < 2000 And iNdx = 0) Then
                  Exit For
               End If
            End If
         Next iNdx

         'If one of the file are not there, county is not ready
         If iNdx = g_CtyList(iCnt).NumFiles Then
            g_CtyList(iCnt).Status = "Y"
            g_CtyList(iCnt).LastFileDate = iTime
         End If
      Next iCnt

checkNewData_Error:
   End Sub

   Private Sub setLastMTime(ByRef iCnt As Integer)
      Dim iRet, iNdx As Integer
      Dim sTmp, sTime As String
      Dim lTime As Integer

      On Error GoTo setLastMTime_Error

      For iNdx = 0 To g_CtyList(iCnt).NumFiles - 1
         sTmp = gInputDir & "\" & g_CtyList(iCnt).CountyCode & "\" & g_CtyList(iCnt).FileName(iNdx)
         If Dir(sTmp) = "" Then
            Exit For
         Else
            sTime = getFileMTime(sTmp)
            lTime = sTime

            If lTime > g_CtyList(iCnt).LastFileDate Then
               g_CtyList(iCnt).LastFileDate = lTime
            End If
         End If
      Next iNdx

setLastMTime_Error:
   End Sub

   Private Sub setLastFileDate(ByRef iCnt As Integer, ByRef sFilename As String)
      Dim sTime As String

      If Dir(sFilename) > "" Then
         sTime = getFileMTime(sFilename)
         g_CtyList(iCnt).LastFileDate = CInt(sTime)
      End If
   End Sub

   Public Function UnzipFile1(ByRef sZipfile As String, ByRef sFolder As String) As Integer
      Dim bRet As Boolean
      Dim iRet As Integer
      Dim doZip As clsZip

      UnzipFile1 = 0
      If Dir(sZipfile) = "" Then
         Exit Function
      Else
         LogMsg("Unzipping " & sZipfile & " to " & sFolder)
      End If

      iRet = -1
      Try
         doZip = New clsZip
         doZip.UnzipFolder = sFolder
         doZip.ZipFilename = sZipfile
         doZip.FilesToZip = "*"
         bRet = doZip.unzipIt(False)
         If bRet = False Then
            LogMsg("***** Error unziping " & sZipfile)
            LogMsg("ErrMsg: " & doZip.getErrMsg())
         Else
            iRet = 0
         End If
      Catch ex As Exception
         LogMsg("***** Exception in Unzipping1(): " & sZipfile)
         LogMsg("ErrMsg: " & ex.Message)
      End Try

      doZip = Nothing
      UnzipFile1 = iRet
   End Function

End Module